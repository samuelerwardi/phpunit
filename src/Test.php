<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class Test extends TestCase
{
    private function subtractedWhile($nums): array
    {
        $max = 0;
        $i = 0;
//        [3, 1, 4, 2]
//        [1, 3, 4, 2]
//        [2, 3, 1, 4]
//        [4, 2, 3, 1]
        while ($i < count($nums)) {
            if ($nums[$i] > $nums[$i + 1]) {
                if ($nums[$i] > $max) {
                    $max = $nums[$i];
                }
            } else {
                if ($nums[$i + 1] > $max) {
                    $max = $nums[$i + 1];
                }
            }
            $i = $i + 2;
        }
        return [$max];
    }

    private function subtracted($nums): array
    {
        $max = 0;
        foreach ($nums as $index => $item) {
            if ($max < $item) {
                $max = $item;
            }
        }
        return [$max];
    }

    public function testSubtracted(): void
    {
        $this->assertSame([4], $this->subtracted([3, 1, 4, 2]));
    }

    public function testSubtractedWhile(): void
    {
        $this->assertSame([4], $this->subtractedWhile([3, 1, 4, 2]));
        $this->assertSame([4], $this->subtractedWhile([2, 3, 1, 4]));
        $this->assertSame([4], $this->subtractedWhile([4, 2, 3, 1]));
        $this->assertSame([4], $this->subtractedWhile([1, 4, 2, 3]));
    }

    private function divided($nums, $number): array
    {
        $result = [];
        foreach ($nums as $value) {
            $match = false;
            foreach ($nums as $item) {
                if (($value / $item) == $number) {
                    $match = true;
                }
            }
            if (!$match) {
                $result[] = $value;
            }
        }
        return $result;
    }

    public function testDivided(): void
    {
        $this->assertSame([1, 2, 3], $this->divided([1, 2, 3, 4], 4));
        $this->assertSame([2, 3], $this->divided([2, 4, 8, 3], 2));
    }

    private function wordLength(string $word, int $length, int $index, $result = []): array
    {
        $explodeString = explode(" ", $word);
        // base
        if ($index == count($explodeString) - 1) {
            if (strlen($explodeString[$index]) == $length) {
                $result[] = $explodeString[$index];
            }
        } else {
            if (strlen($explodeString[$index]) == $length) {
                $result[] = $explodeString[$index];
            }
            return $this->wordLength($word, $length, $index + 1, $result);
        }
        return $result;
    }

    public function testStringLength(): void
    {
        // question
        $this->assertSame(["loud", "four", "lost"], $this->wordLength("souvenir loud four lost", 4, 0));
        $this->assertSame(3, count($this->wordLength("loud four lost souvenir", 4, 0)));
        $this->assertSame(3, count($this->wordLength("four lost souvenir loud", 4, 0)));
        $this->assertSame(3, count($this->wordLength("lost souvenir loud four", 4, 0)));
    }
}